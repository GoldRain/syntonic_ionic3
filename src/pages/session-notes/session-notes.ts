import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the SessionNotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-session-notes',
    templateUrl: 'session-notes.html',
})
export class SessionNotesPage {

    notes = [];
    note;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SessionNotesPage');
    }

    closeModal() {
        this.viewCtrl.dismiss(this.notes);
    }

    save() {
        var NtimeObject = new Date();
        var Ntoday = new Date(NtimeObject.getTime());

        let session_not = {
            'text': this.note,
            'timestamp': new Date(new Date().getTime()),
            'datetime': Ntoday.getFullYear() + "/" + (Ntoday.getMonth() + 1) + "/" + (Ntoday.getDate() + 1) + " " + Ntoday.getHours() + ":" + Ntoday.getMinutes() + ":" + Ntoday.getSeconds()
        };

        this.notes.push(session_not);
        this.note = '';
    }

}
