import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SessionNotesPage } from './session-notes';

@NgModule({
  declarations: [
    SessionNotesPage,
  ],
  imports: [
    IonicPageModule.forChild(SessionNotesPage),
  ],
})
export class SessionNotesPageModule {}
