import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserRegistrationService} from "../../providers/userRegistration.service";
import {HomePage} from '../../pages/home/home';

/**
 * Generated class for the ConfirmRegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-confirm-registration',
    templateUrl: 'confirm-registration.html',
})
export class ConfirmRegistrationPage {

    user;
    loading;
    account: FormGroup;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public toastCtrl: ToastController,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public userRegistrationService: UserRegistrationService) {
        this.user = navParams.get('user');

        this.account = new FormGroup({
            username: new FormControl('', Validators.required),
            code: new FormControl('', Validators.required),
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ConfirmRegistrationPage');
    }

    verify() {
        if (this.account.valid) {
            this.showLoading();
            this.userRegistrationService.confirmRegistration(this.user.email, this.account.value.code, this);
        }
    }

    cognitoCallback(message: string, result: any) {
        if (message != null) { //error
            let toast = this.toastCtrl.create({
                message: 'Account verification failed.',
                duration: 3000,
                position: 'top',
                cssClass: 'error'
            });
            toast.present();
            this.hideLoading();
        } else { //success
            this.hideLoading();
            this.navCtrl.setRoot(HomePage);
            // this.userRegistrationService.create({
            //     "emailId" : this.user.email,
            //     "roleName" : this.user.role || 'Clinician',
            //     "roleId" : this.user.role_id || 'Clinician'
            // }).map(res => res.json()).subscribe((resp) => {
            //     this.hideLoading();
            //     this.navCtrl.setRoot(HomePage);
            // }, (err) => {
            //     this.hideLoading();
            //     let toast = this.toastCtrl.create({
            //         message: 'Login failed.',
            //         duration: 3000,
            //         position: 'top',
            //         cssClass: 'error'
            //     });
            //     toast.present();
            // });
        }
    }

    resendCode() {
        this.showLoading();
        this.userRegistrationService.resendCode(this.user.email, this);
    }

    resendCodeCallback(message: string, result: any) {
        if (message != null) { //error
            let toast = this.toastCtrl.create({
                message: 'Account verification failed.',
                duration: 3000,
                position: 'top',
                cssClass: 'error'
            });
            toast.present();
            this.hideLoading();
        } else { //success
            this.hideLoading();
            this.navCtrl.push('ConfirmRegistrationPage', {
                'user': this.user
            });
        }
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });

        this.loading.present();
    }

    hideLoading() {
        if (!this.loading) return;
        this.loading.dismiss();
        this.loading = null;
    }

}
