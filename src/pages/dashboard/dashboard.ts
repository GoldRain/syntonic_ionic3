import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserLoginService} from "../../providers/userLogin.service";
import {ChildrenService} from "../../providers/children.service";

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-dashboard',
    templateUrl: 'dashboard.html',
})
export class DashboardPage {

    userinfo;
    childrens;
    category;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public userService: UserLoginService, public childrenService: ChildrenService) {
        this.category = 'record';
        this.getChildrens();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DashboardPage');
    }

    getChildrens() {
        this.childrenService.getChildrens().subscribe((res: any) => {
            this.childrens = JSON.parse(res.body);
        }, (err) => {
            console.error(err);
        });
    }

    segmentChanged() {
        this.category != this.category;
    }

    session(children: any) {
        if (this.category === 'record') {
            this.navCtrl.push('SessionPage', {'children': children});
        } else {
            this.navCtrl.push('ReviewSessionInfoPage', {'children': children});
        }
    }
}
