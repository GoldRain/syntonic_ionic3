import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ReviewSessionInfoPage} from "../review-session-info/review-session-info";
import {SessionService} from "../../providers/session.service";

/**
 * Generated class for the EditSessionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-edit-session',
    templateUrl: 'edit-session.html',
})
export class EditSessionPage {

    session;
    sessiondate;

    constructor(public navCtrl: NavController, public navParams: NavParams, public sessionService: SessionService) {
        this.session = navParams.get('session');

        let year = this.session.sessionDate.substr(0, 2);
        let month = this.session.sessionDate.substr(2, 2);
        let day = this.session.sessionDate.substr(4, 4);
        this.sessiondate = year + "/" + month + "/" + day;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad EditSessionPage');
    }

    save() {
        this.sessionService.update(this.session.clinicianId, this.session.sessionId, this.session).subscribe((res: any) => {
            this.navCtrl.push(ReviewSessionInfoPage, {});
        }, (err) => {
            console.error(err);
        });
    }
}
