import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, AlertController} from 'ionic-angular';
import {SessionReviewPage} from "../session-review/session-review";
import {SessionNotesPage} from "../session-notes/session-notes";
import {DashboardPage} from "../dashboard/dashboard";
import {UserLoginService} from "../../providers/userLogin.service";
import {SessionService} from "../../providers/session.service";

/**
 * Generated class for the RecordSessionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-record-session',
    templateUrl: 'record-session.html',
})
export class RecordSessionPage {

    children;
    behaviors;
    sessioninfo;
    userinfo;
    sessions = [];
    summerizer = 0;
    note;
    session_notes = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserLoginService,
                public modalCtrl: ModalController, public alertCtrl: AlertController, public sessionService: SessionService) {
        this.children = navParams.get('children');
        this.behaviors = navParams.get('behaviors');
        this.sessioninfo = navParams.get('session-info');
        this.userinfo = this.userService.getUser();

        for (let i = 0; i < this.behaviors.length; i++) {
            this.behaviors[i].buttonLabel = "Start";
            this.behaviors[i].buttonLabelReset = "No";
            this.behaviors[i].hr = 0;
            this.behaviors[i].min = 0;
            this.behaviors[i].second = 0;
            this.behaviors[i].mili = 0;
            this.behaviors[i].str_hr = "00";
            this.behaviors[i].str_min = "00";
            this.behaviors[i].str_sec = "00";
            this.behaviors[i].str_mil = "00";
            this.behaviors[i].review = false;
        }
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RecordSessionPage');
    }

    generateCurrentTime() {
        var today = new Date();
        var hrs = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();
        var strHrs = '' + hrs;
        var strMin = '' + min;
        var strSec = '' + sec;

        if (hrs < 10) {
            strHrs = '0' + hrs;
        }

        if (min < 10) {
            strMin = '0' + min;
        }

        if (sec < 10) {
            strSec = '0' + sec;
        }

        return strHrs + ":" + strMin + ":" + strSec;
    };

    addSession(index) {
        let session = {
            startTime: this.behaviors[index].startTime,
            stopTime: this.behaviors[index].stopTime,
            timeInterval: this.behaviors[index].timeInterval,
            behaviorName: this.behaviors[index].behaviorName,
            review: this.behaviors[index].review,
            timestamp: Math.floor(Date.now())
        };

        this.sessions.push(session);
    };

    addButtonStatus(value) {
        this.summerizer = this.summerizer + value;
    };

    // Start and stop session
    applyStopwatch(index) {
        let self = this;
        if (this.behaviors[index].buttonLabel === 'Start') {
            this.addButtonStatus(1);
            this.behaviors[index].review = false;

            var startTime = this.generateCurrentTime();

            this.behaviors[index].startTime = startTime;

            this.behaviors[index].buttonLabel = "Stop";

            var ssTime = new Date();
            this.behaviors[index].timeinterval = setInterval(
                function () {
                    var now = new Date();
                    let s = now.getTime() - ssTime.getTime();
                    self.behaviors[index].mili = s % 1000;
                    s = (s - self.behaviors[index].mili) / 1000;
                    self.behaviors[index].second = s % 60;
                    s = (s - self.behaviors[index].second) / 60;
                    self.behaviors[index].min = s % 60;
                    self.behaviors[index].hr = (s - self.behaviors[index].min) / 60;

                    self.behaviors[index].str_hr = self.behaviors[index].hr;
                    self.behaviors[index].str_min = self.behaviors[index].min;
                    self.behaviors[index].str_sec = self.behaviors[index].second;
                    self.behaviors[index].str_mil = self.behaviors[index].mili;

                    if (self.behaviors[index].mili < 100) {
                        self.behaviors[index].str_mil = "0" + self.behaviors[index].str_mil;
                    }

                    if (self.behaviors[index].second < 10) {
                        self.behaviors[index].str_sec = "0" + self.behaviors[index].str_sec;
                    }

                    if (self.behaviors[index].min < 10) {
                        self.behaviors[index].str_min = "0" + self.behaviors[index].str_min;
                    }

                    if (self.behaviors[index].hr < 10) {
                        self.behaviors[index].str_hr = "0" + self.behaviors[index].str_hr;
                    }

                }, 31
            );
        }
        else if (this.behaviors[index].buttonLabel === 'Stop') //has to stop
        {

            this.addButtonStatus(-1);
            this.behaviors[index].buttonLabel = "Start";

            clearInterval(this.behaviors[index].timeinterval);

            let stopTime = this.generateCurrentTime();

            this.behaviors[index].stopTime = stopTime;
            this.behaviors[index].timeInterval = this.behaviors[index].str_hr +
                ":" + this.behaviors[index].str_min +
                ":" + this.behaviors[index].str_sec +
                ":" + this.behaviors[index].str_mil;

            this.addSession(index);
            this.applyReset(index, false);

        }

    };

    // Reset session
    applyReset(index, directReset) {
        this.behaviors[index].buttonLabelReset = "Yes";
        this.behaviors[index].hr = 0;
        this.behaviors[index].min = 0;
        this.behaviors[index].second = 0;
        this.behaviors[index].mili = 0;
        this.behaviors[index].str_hr = "00";
        this.behaviors[index].str_min = "00";
        this.behaviors[index].str_sec = "00";
        this.behaviors[index].str_mil = "00";
        this.behaviors[index].buttonLabelReset = "No";
        //delete API behavior timing
        this.behaviors[index].startTime = '';
        this.behaviors[index].stopTime = '';
        this.behaviors[index].timeInterval = '';

        if (directReset) {
            let alert = this.alertCtrl.create({
                title: 'Confirmation',
                message: 'Do you want to delete the last entry?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'OK',
                        handler: () => {
                            let name = this.behaviors[index].behaviorName;
                            let count = (this.sessions.length - 1);
                            for (let i = count; i >= 0; i--) {
                                if (this.sessions[i].behaviorName === name) {
                                    this.sessions.splice(i, 1);
                                    break;
                                }
                            }
                        }
                    }
                ]
            });
            alert.present();
        }
    };

    review() {
        let reviewModal = this.modalCtrl.create(SessionReviewPage, {
            'sessions': this.sessions,
            'session-info': this.sessioninfo
        });
        reviewModal.present();
    }

    notes() {
        let notesModal = this.modalCtrl.create(SessionNotesPage, {
            'sessions': this.sessions,
            'session-info': this.sessioninfo
        });
        notesModal.onDidDismiss(data => {
            this.session_notes = data;
        });
        notesModal.present();
    }

    // save session
    save() {
        let alert = this.alertCtrl.create({
            title: 'Confirmation',
            message: 'Do you want to exit the session?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'OK',
                    handler: () => {
                        if (this.sessions) {
                            let param = {
                                "sessionName": this.sessioninfo.name,
                                "childInfoId": this.children.childInfoId,
                                "preSessionInfo": this.sessioninfo.info,
                                "default": "D",
                                "clinicianName": this.children.clinicianId,
                                "sessionDate": this.sessioninfo.date,
                                "startTime": this.sessioninfo.time,
                                "endTime": this.generateCurrentTime(),
                                "sessionNotes": this.session_notes,
                                "behaviorRecord": this.sessions
                            };

                            this.sessionService.save(param).map(res => res.json()).subscribe((resp) => {
                                this.navCtrl.setRoot(DashboardPage);
                            }, (err) => {

                            });
                        } else {
                            this.navCtrl.setRoot(DashboardPage, {});
                        }
                    }
                }
            ]
        });
        alert.present();
    }
}
