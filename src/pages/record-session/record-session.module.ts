import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecordSessionPage } from './record-session';

@NgModule({
  declarations: [
    RecordSessionPage,
  ],
  imports: [
    IonicPageModule.forChild(RecordSessionPage),
  ],
})
export class RecordSessionPageModule {}
