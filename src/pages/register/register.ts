import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserRegistrationService} from "../../providers/userRegistration.service";
import {CognitoCallback} from "../../providers/cognito.service";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage implements CognitoCallback {

    account: FormGroup;
    loading;

    constructor(public navCtrl: NavController,
                public toastCtrl: ToastController,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public userRegistrationService: UserRegistrationService) {

        this.account = new FormGroup({
            email: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
            name: new FormControl(),
            family_name: new FormControl(),
            role: new FormControl(),
            role_id: new FormControl(),
            gender: new FormControl(),
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }

    register() {
        if (this.account.valid) {
            this.showLoading();
            this.userRegistrationService.register(this.account.value, this);
        }
    }

    cognitoCallback(message: string, result: any) {
        if (message != null) { //error
            let toast = this.toastCtrl.create({
                message: 'Account verification failed.',
                duration: 3000,
                position: 'top',
                cssClass: 'error'
            });
            toast.present();
            this.hideLoading();
        } else { //success
            this.hideLoading();
            this.navCtrl.push('ConfirmRegistrationPage', {
                'user': this.account.value
            });
        }
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });

        this.loading.present();
    }

    hideLoading() {
        if (!this.loading) return;
        this.loading.dismiss();
        this.loading = null;
    }
}
