import {Component} from '@angular/core';
import {NavController, ToastController, AlertController, LoadingController} from 'ionic-angular';
import {UserLoginService} from "../../providers/userLogin.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {RegisterPage} from "../register/register";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    account: FormGroup;
    loading;

    constructor(public navCtrl: NavController,
                public toastCtrl: ToastController,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public userService: UserLoginService) {

        this.account = new FormGroup({
            email: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
        });
    }

    ionViewLoaded() {
        console.log("Checking if the user is already authenticated. If so, then redirect to the secure site");
        this.userService.isAuthenticated(this);
    }

    login() {
        if (this.account.valid) {
            this.showLoading();
            this.userService.authenticate(this.account.value, this);
        }
    }

    cognitoCallback(message: string, result: any) {
        if (message != null) { //error
            let toast = this.toastCtrl.create({
                message: 'Login failed.',
                duration: 3000,
                position: 'top',
                cssClass: 'error'
            });
            toast.present();
            this.hideLoading();
        } else { //success
            this.hideLoading();
            this.navCtrl.setRoot('DashboardPage');
        }
    }

    isLoggedInCallback(message: string, isLoggedIn: boolean) {
        if (isLoggedIn) {
            this.navCtrl.setRoot('DashboardPage');
        }
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });

        this.loading.present();
    }

    hideLoading() {
        if (!this.loading) return;
        this.loading.dismiss();
        this.loading = null;
    }

    register() {
        this.navCtrl.push(RegisterPage);
    }
}
