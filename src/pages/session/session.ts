import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SessionService} from "../../providers/session.service";
import {RecordSessionPage} from "../record-session/record-session";

/**
 * Generated class for the SessionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-session',
    templateUrl: 'session.html',
})
export class SessionPage {

    children;
    behaviors;
    selectedArray: any = [];
    session: FormGroup;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sessionService: SessionService) {
        this.children = navParams.get('children');

        this.session = new FormGroup({
            name: new FormControl('', Validators.required),
            info: new FormControl('', Validators.required)
        });

        this.getBehaviors();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SessionPage');
    }

    getBehaviors() {
        this.sessionService.getBehaviors().subscribe((res: any) => {
            this.behaviors = JSON.parse(res.body);
            for (let i = 0; i < this.behaviors.length; i++) {
                this.behaviors[i].checked = false;
            }
        }, (err) => {
            console.error(err);
        });
    }

    selectBehavior(data) {
        if (data.checked == true) {
            this.selectedArray.push(data);
        } else {
            let newArray = this.selectedArray.filter(function (el) {
                return el.behaviorId !== data.behaviorId;
            });
            this.selectedArray = newArray;
        }
    }

    record() {
        let today = new Date(new Date().getTime());

        let month = today.getMonth() + 1;
        let day = today.getDate();
        let year = today.getFullYear();

        let strMonth = '' + month;
        let strDay = '' + day;
        let strYear = '' + year;
        let strHrs = '' + today.getHours();
        let strMins = '' + today.getMinutes();
        let strSec = '' + today.getSeconds();

        if (month < 10) {
            strMonth = '0' + month;
        }

        if (day < 10) {
            strDay = '0' + day;
        }

        if (today.getHours() < 10) {
            strHrs = '0' + today.getHours();
        }

        if (today.getMinutes() < 10) {
            strMins = '0' + today.getMinutes();
        }

        if (today.getSeconds() < 10) {
            strSec = '0' + today.getSeconds();
        }

        this.session.value.date = strMonth + strDay + strYear;
        this.session.value.time = strHrs + ":" + strMins + ":" + strSec;

        this.navCtrl.setRoot(RecordSessionPage, {
            'children': this.children,
            'behaviors': this.selectedArray,
            'session-info': this.session.value
        });
    }
}
