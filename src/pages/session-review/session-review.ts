import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the SessionReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-session-review',
    templateUrl: 'session-review.html',
})
export class SessionReviewPage {

    sessions;
    sessioninfo;
    date;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
        this.sessions = navParams.get('sessions');
        this.sessioninfo = navParams.get('session-info');
        console.log(this.sessioninfo);
        this.date = new Date();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SessionReviewPage');
    }

    closeModal() {
        this.viewCtrl.dismiss();
    }

}
