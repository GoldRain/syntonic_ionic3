import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SessionReviewPage } from './session-review';

@NgModule({
  declarations: [
    SessionReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(SessionReviewPage),
  ],
})
export class SessionReviewPageModule {}
