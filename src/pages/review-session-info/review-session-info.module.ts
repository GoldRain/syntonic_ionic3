import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReviewSessionInfoPage } from './review-session-info';

@NgModule({
  declarations: [
    ReviewSessionInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(ReviewSessionInfoPage),
  ],
})
export class ReviewSessionInfoPageModule {}
