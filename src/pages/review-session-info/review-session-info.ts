import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {SessionService} from "../../providers/session.service";
import {EditSessionPage} from "../edit-session/edit-session";

/**
 * Generated class for the ReviewSessionInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-review-session-info',
    templateUrl: 'review-session-info.html',
})
export class ReviewSessionInfoPage {

    children;
    sessions;
    sessioninfo;

    constructor(public navCtrl: NavController, public navParams: NavParams, public sessionService: SessionService) {
        this.children = navParams.get('children');
        this.getSessions();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ReviewSessionInfoPage');
    }

    getSessions() {
        this.sessionService.getSessions(this.children.clinicianId).subscribe((res: any) => {
            this.sessions = res;
            this.sessioninfo = this.sessions[0];
            console.log(this.sessioninfo);
        }, (err) => {
            console.error(err);
        });
    }

    deleteBehavior(sessionIndex, behaviorIndex) {
        this.sessions[sessionIndex].behaviorRecord.splice(behaviorIndex, 1);
        this.sessionService.update(this.children.clinicianId, this.sessions[sessionIndex].sessionId, this.sessions[sessionIndex].behaviorRecord).subscribe((res: any) => {
            this.getSessions();
        }, (err) => {
            console.error(err);
        });
    }

    edit(session) {
        this.navCtrl.push(EditSessionPage, {'session': session});
    }

}
