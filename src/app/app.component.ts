import {Component, ViewChild} from '@angular/core';
import {Events, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AwsUtil} from "../providers/aws.service";
import {UserLoginService} from "../providers/userLogin.service";

import {HomePage} from '../pages/home/home';
import {DashboardPage} from '../pages/dashboard/dashboard';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any = null;
    @ViewChild(Nav) nav: Nav;

    constructor(public platform: Platform,
                public statusBar: StatusBar,
                public splashScreen: SplashScreen,
                public awsUtil: AwsUtil,
                public userService: UserLoginService,
                public events: Events) {
        this.platform.ready().then(() => {
            this.awsUtil.initAwsService();
            this.rootPage = HomePage;
            this.splashScreen.hide();
        });

        userService.checkAuth().then((authenticated) => {
            if (authenticated) {
                this.rootPage = DashboardPage;
            } else {
                this.rootPage = HomePage;
            }
        }).catch(() => {
            this.rootPage = HomePage;
        });
    }

    logout() {
        this.userService.logout().then(() => {
            this.nav.setRoot(HomePage);
        });
    }
}

