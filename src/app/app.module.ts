import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicStorageModule} from '@ionic/storage';
import {CognitoUtil} from "../providers/cognito.service";
import {AwsUtil} from "../providers/aws.service";
import {UserLoginService} from "../providers/userLogin.service";
import {UserRegistrationService} from "../providers/userRegistration.service";
import {ChildrenService} from "../providers/children.service";
import {SessionService} from "../providers/session.service";

import {MyApp} from './app.component';
import {HomePageModule} from '../pages/home/home.module';
import {RegisterPageModule} from '../pages/register/register.module';
import {ConfirmRegistrationPageModule} from '../pages/confirm-registration/confirm-registration.module';
import {DashboardPageModule} from '../pages/dashboard/dashboard.module';
import {SessionPageModule} from '../pages/session/session.module';
import {RecordSessionPageModule} from '../pages/record-session/record-session.module';
import {SessionReviewPageModule} from '../pages/session-review/session-review.module';
import {SessionNotesPageModule} from '../pages/session-notes/session-notes.module';
import {ReviewSessionInfoPageModule} from '../pages/review-session-info/review-session-info.module';
import {EditSessionPageModule} from '../pages/edit-session/edit-session.module';

import {Api} from '../providers/api';

@NgModule({
    declarations: [
        MyApp
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp, {
            tabsHideOnSubPages: true,
            scrollAssist: false,
            autoFocusAssist: false,
            // scrollPadding:true
        }),
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        HomePageModule,
        RegisterPageModule,
        ConfirmRegistrationPageModule,
        DashboardPageModule,
        SessionPageModule,
        RecordSessionPageModule,
        SessionReviewPageModule,
        SessionNotesPageModule,
        ReviewSessionInfoPageModule,
        EditSessionPageModule,
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Api,
        CognitoUtil,
        AwsUtil,
        UserLoginService,
        UserRegistrationService,
        ChildrenService,
        SessionService,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
