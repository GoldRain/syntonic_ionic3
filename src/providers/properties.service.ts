export let _REGION = "us-east-1";

export let _IDENTITY_POOL_ID = "us-east-1:d147511c-d9ba-48d1-92ed-b25c52e9f842";
export let _USER_POOL_ID = "us-east-1_VE9HLRmw7";
export let _CLIENT_ID = "2bbad694hd4ors8v6sij8j8gpv";

export let _POOL_DATA = {
    UserPoolId: _USER_POOL_ID,
    ClientId: _CLIENT_ID
};
