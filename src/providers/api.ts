import {Injectable} from '@angular/core';
import {Http, RequestOptions, URLSearchParams, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {AppSettings} from "../app/app.settings";

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
    url: string = AppSettings.API_ENDPOINT;
    token: string = null;

    constructor(public http: Http) {
    }

    get(endpoint: string, params?: any, options?: RequestOptions) {
        if (!options) {
            options = new RequestOptions();
        }

        // Support easy query params for GET requests
        if (params) {
            let p = new URLSearchParams();
            for (let k in params) {
                p.set(k, params[k]);
            }
            // Set the search field if we have params and don't already have
            // a search field set in options.
            options.params = !options.params && p || options.params;
        }
        return this.http.get(this.url + '/' + endpoint, options);
    }

    post(endpoint: string, body: any, options?: RequestOptions) {
        if (!options) {
            options = new RequestOptions();
        }

        return this.http.post(this.url + '/' + endpoint, body, options);
    }

    put(endpoint: string, body: any, options?: RequestOptions) {
        if (!options) {
            options = new RequestOptions();
        }

        if (this.token) {
            options.headers = new Headers({'X-auth': this.token});
        }

        return this.http.put(this.url + '/' + endpoint, body, options);
    }

    delete(endpoint: string, options?: RequestOptions) {
        if (!options) {
            options = new RequestOptions();
        }

        if (this.token) {
            options.headers = new Headers({'X-auth': this.token});
        }

        return this.http.delete(this.url + '/' + endpoint, options);
    }

    patch(endpoint: string, body: any, options?: RequestOptions) {
        if (!options) {
            options = new RequestOptions();
        }

        if (this.token) {
            options.headers = new Headers({'X-auth': this.token});
        }

        return this.http.put(this.url + '/' + endpoint, body, options);
    }
}
