import {Callback, CognitoCallback, CognitoUtil, RegistrationUser, ResendCodeCallback} from "./cognito.service";
import {Injectable} from "@angular/core";
import {Api} from "./api";

declare let AWS: any;
declare let AWSCognito: any;

@Injectable()
export class UserRegistrationService {
    constructor(public cUtil: CognitoUtil, public api: Api) {
    }

    register(user: any, callback: CognitoCallback): void {
        let attributeList = [];

        let dataEmail = {Name: 'email', Value: user.email};
        let dataName = {Name: 'name', Value: user.name};
        let dataFamilyName = {Name: 'family_name', Value: user.family_name};
        let dataRole = {Name: 'custom:role', Value: user.role};
        let dataRoleId = {Name: 'custom:roleId', Value: user.role_id};
        let dataGender = {Name: 'gender', Value: user.gender};
        let dataPhone = {Name: 'phone_number', Value: '+14325551212'};

        attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail));
        attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataName));
        attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataFamilyName));
        attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataRole));
        attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataRoleId));
        attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataGender));
        attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataPhone));

        this.cUtil.getUserPool().signUp(user.email, user.password, attributeList, null, function (err, result) {
            if (err) {
                console.log(err.message);
                callback.cognitoCallback(err.message, null);
            } else {
                console.log("registered user: " + result);
                callback.cognitoCallback(null, result);
            }
        });

    }

    confirmRegistration(username: string, confirmationCode: string, callback: CognitoCallback): void {

        let userData = {
            Username: username,
            Pool: this.cUtil.getUserPool()
        };

        let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

        cognitoUser.confirmRegistration(confirmationCode, true, function (err, result) {
            if (err) {
                callback.cognitoCallback(err.message, null);
            } else {
                callback.cognitoCallback(null, result);
            }
        });
    }

    resendCode(username: string, callback: ResendCodeCallback): void {
        let userData = {
            Username: username,
            Pool: this.cUtil.getUserPool()
        };

        let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

        cognitoUser.resendConfirmationCode(function (err, result) {
            if (err) {
                callback.resendCodeCallback(err.message, null);
            } else {
                callback.resendCodeCallback(null, result);
            }
        });
    }

    create(accountInfo: any) {
        let seq = this.api.post('user', accountInfo).share();

        seq
            .map(res => res.json())
            .subscribe(res => {
                console.log(res);
            }, err => {
                console.error('ERROR', err);
            });

        return seq;
    }

}