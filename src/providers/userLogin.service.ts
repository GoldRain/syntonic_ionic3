import {Injectable} from "@angular/core";
import {CognitoCallback, CognitoUtil, LoggedInCallback} from "./cognito.service";

declare let AWSCognito: any;
import {Storage} from '@ionic/storage';

@Injectable()
export class UserLoginService {

    private _token: string;
    private _user: any;
    private _authenticated: boolean = false;

    constructor(public cUtil: CognitoUtil, public storage: Storage) {

    }

    getUser() {
        return this._user;
    }

    getToken() {
        return this._token;
    }

    authenticate(account: any, callback: CognitoCallback) {
        let self = this;

        // Need to provide placeholder keys unless unauthorised user access is enabled for user pool
        AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'})

        let authenticationData = {
            Username: account.email,
            Password: account.password,
        };
        let authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);

        let userData = {
            Username: account.email,
            Pool: this.cUtil.getUserPool()
        };

        console.log("Authenticating the user");
        let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                self._token = result.idToken.jwtToken;
                self.getParameters().then((authenticated) => {
                    if (authenticated) {
                        callback.cognitoCallback(null, result);
                    } else {
                        callback.cognitoCallback(null, result);
                    }
                }).catch(() => {
                    callback.cognitoCallback(null, result);
                });
            },
            onFailure: function (err) {
                callback.cognitoCallback(err.message, null);
            },
        });
    }

    isAuthenticated(callback: LoggedInCallback) {
        if (callback == null)
            throw("Callback in isAuthenticated() cannot be null");

        console.log("Getting the current user");
        let cognitoUser = this.cUtil.getCurrentUser();

        if (cognitoUser != null) {
            cognitoUser.getSession(function (err, session) {
                if (err) {
                    console.log("Couldn't get the session: " + err, err.stack);
                    callback.isLoggedInCallback(err, false);
                }
                else {
                    console.log("Session is valid: " + session.isValid());
                    callback.isLoggedInCallback(err, session.isValid());
                }
            });
        } else {
            callback.isLoggedInCallback("Can't retrieve the CurrentUser", false);
        }
    }

    /**
     * Checks if user is logged in or not
     *
     * @returns {Promise<boolean>}
     */
    checkAuth(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.storage.keys().then((keys) => {
                if (keys.indexOf('_auth_data') !== -1) {
                    this.storage.get('_auth_data').then((auth_data) => {
                        this._user = auth_data.data;
                        this._token = auth_data.token;
                        this._authenticated = true;

                        resolve(true);
                    });
                } else {
                    resolve(false);
                }
            }).catch(() => {
                resolve(false);
            });
        });
    }

    logout() {
        this.cUtil.getCurrentUser().signOut();
        return this.storage.remove('_auth_data');
    }

    _loggedIn(resp) {
        this._user = resp;
        return this.storage.set('_auth_data', {data: resp, token: this._token});
    }

    getParameters(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            let self = this;
            let cognitoUser = this.cUtil.getCurrentUser();

            if (cognitoUser != null) {
                cognitoUser.getSession(function (err, session) {
                    if (err)
                        console.log("Couldn't retrieve the user");
                    else {
                        cognitoUser.getUserAttributes(function (err, result) {
                            if (err) {
                                resolve(false);
                            } else {
                                let user = {
                                    'email': '',
                                    'name': '',
                                    'role': '',
                                    'roleId': ''
                                };

                                for (let i = 0; i < result.length; i++) {
                                    console.log('attribute ' + result[i].getName() + ' has value ' + result[i].getValue());
                                    if (result[i].getName() === 'email') {
                                        user.email = result[i].getValue();
                                    }
                                    if (result[i].getName() === 'name') {
                                        user.name = result[i].getValue();
                                    }
                                    if (result[i].getName() === 'custom:role') {
                                        user.role = result[i].getValue();
                                    }
                                    if (result[i].getName() === 'custom:roleId') {
                                        user.roleId = result[i].getValue();
                                    }
                                }

                                self._loggedIn(user);
                                resolve(true);
                            }
                        });
                    }

                });
            } else {
                resolve(false);
            }
        });
    }
}