import {Injectable} from '@angular/core';
import {Api} from "./api";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share';

@Injectable()
export class SessionService {

    constructor(public api: Api) {

    }

    getBehaviors() {
        return this.api.get('behaviors').share().map(res => res.json());
    }

    save(sessionInfo: any) {
        let seq = this.api.post('clinician/' + sessionInfo.clinicianName + '/session', sessionInfo).share();

        seq
            .map(res => res.json())
            .subscribe(res => {
                console.log(res);
            }, err => {
                console.error('ERROR', err);
            });

        return seq;
    }

    getSessions(clinicianId) {
        return this.api.get('clinician/' + clinicianId + '/sessions').share().map(res => res.json());
    }

    update(clinicianId, sessionId, param) {
        let seq = this.api.put('clinician/' + clinicianId + '/session/' + sessionId, param).share();

        seq
            .map(res => res.json())
            .subscribe(res => {
                console.log(res);
            }, err => {
                console.error('ERROR', err);
            });

        return seq;
    }
}
