import {Injectable} from '@angular/core';
import {Api} from "./api";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share';

@Injectable()
export class ChildrenService {

    constructor(public api: Api) {

    }

    getChildrens() {
        return this.api.get('children').share().map(res => res.json());
    }
}
